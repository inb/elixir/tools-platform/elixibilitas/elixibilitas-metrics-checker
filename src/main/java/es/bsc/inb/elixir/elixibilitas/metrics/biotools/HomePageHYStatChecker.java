/**
 * *****************************************************************************
 * Copyright (C) 2020 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.elixibilitas.metrics.biotools;

import es.bsc.inb.elixir.elixibilitas.dao.MetricsDAO;
import es.bsc.inb.elixir.elixibilitas.dao.ToolsDAO;
import es.bsc.inb.elixir.elixibilitas.metrics.MetricsChecker;
import es.bsc.inb.elixir.openebench.model.metrics.HttpStatusCounter;
import es.bsc.inb.elixir.openebench.model.metrics.Metrics;
import es.bsc.inb.elixir.openebench.model.metrics.Project;
import es.bsc.inb.elixir.openebench.model.metrics.Website;
import es.bsc.inb.elixir.openebench.model.tools.Tool;
import es.bsc.inb.elixir.openebench.repository.OpenEBenchEndpoint;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import static java.time.temporal.ChronoUnit.DAYS;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.json.JsonArray;
import javax.json.JsonObject;

/**
 * @author Dmitry Repchevsky
 */

public class HomePageHYStatChecker implements MetricsChecker {

    @Override
    public Boolean check(ToolsDAO toolsDAO, MetricsDAO metricsDAO, Tool tool, Metrics metrics) {
        
        final String id = tool.id.toString().substring(OpenEBenchEndpoint.TOOL_URI_BASE.length());
        
        final List<HttpStatusCounter> homepage_hy_stat = getHomepageHYStatistics(metricsDAO, id);
        if (homepage_hy_stat == null) {
            return false;
        }
        
        Website website;
        Project project = metrics.getProject();
        if (project == null) {
            website = new Website();
            project = new Project();
            project.setWebsite(website);
            metrics.setProject(project);
        } else {            
            website = project.getWebsite();
            if (website == null) {
                project.setWebsite(website = new Website());
            }
        }

        website.setHalfYearStatistics(homepage_hy_stat);
        
        return true;
    }
    
    private List<HttpStatusCounter> getHomepageHYStatistics(MetricsDAO metricsDAO, String id) {
        final LocalDate hy_ago_date = LocalDate.now().minusMonths(6).plusDays(1);
        
        final JsonArray operational = metricsDAO.findLog(id, "/project/website/operational", null, null, null);        
        if (operational == null || operational.isEmpty()) {
            return null;
        }
        
        Map<Integer, List<Integer>> serie_map = new HashMap();
        
        LocalDate c_date;
        LocalDate l_date = LocalDate.now();
        
        int j = operational.size() - 1;
        do {
            final JsonObject obj = operational.getJsonObject(j);
            final String code = obj.getString("value", "0");
            final String date = obj.getString("date", null);
            
            c_date = ZonedDateTime.parse(date).toLocalDate();
            try {
                final int c = Integer.parseInt(code);
                List<Integer> serie_list = serie_map.get(c);
                if (serie_list == null) {
                    serie_map.put(c, serie_list = new ArrayList());
                }
                final int days = (int)DAYS.between(c_date, l_date);
                final int max_days = (int)DAYS.between(hy_ago_date, l_date);
                
                serie_list.add(Math.min(days, max_days));
                
                l_date = c_date;
            } catch(NumberFormatException ex) {}
        } while (j-- > 0 && hy_ago_date.isBefore(c_date));

        final List<HttpStatusCounter> homepage_hy_stat = new ArrayList();
        for (Map.Entry<Integer, List<Integer>> entry : serie_map.entrySet()) {
            final HttpStatusCounter c = new HttpStatusCounter();

            c.setStatus(entry.getKey());
            
            final List<Integer> serie_list = entry.getValue();
            if (serie_list != null) {
                Collections.reverse(serie_list);
                c.setDays(serie_list.stream().reduce(0, Integer::sum));
                c.setSerie(serie_list.stream().mapToInt(Integer::intValue).toArray());
            }
            
            homepage_hy_stat.add(c);
        }
        
        return homepage_hy_stat;
    }
}
