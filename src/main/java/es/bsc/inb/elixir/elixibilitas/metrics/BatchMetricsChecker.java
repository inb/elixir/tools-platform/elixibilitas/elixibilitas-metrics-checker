/**
 * *****************************************************************************
 * Copyright (C) 2020 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.elixibilitas.metrics;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.model.Sorts;
import es.bsc.inb.elixir.elixibilitas.dao.MetricsDAO;
import es.bsc.inb.elixir.elixibilitas.dao.ToolsDAO;
import es.bsc.inb.elixir.openebench.model.metrics.Metrics;
import es.bsc.inb.elixir.openebench.model.tools.Tool;
import es.bsc.inb.elixir.openebench.repository.OpenEBenchEndpoint;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Dmitry Repchevsky
 */

public class BatchMetricsChecker {

    private final static String HELP = "java -jar metrics_checker.jar -uri uri\n\n" +
                                       "parameters:\n\n" +
                                       "-uri - mongodb url\n";

    private final ExecutorService executor;
    
    public BatchMetricsChecker(ExecutorService executor) {
        this.executor = executor;
    }
    
    public static void main(String[] args) {
        Map<String, List<String>> params = parameters(args);

        final ExecutorService executor = new ThreadPoolExecutor(32, 32, 0L, TimeUnit.MILLISECONDS, 
                                          new ArrayBlockingQueue<>(1), 
                                          new RejectedExecutionHandler() {
            @Override
            public void rejectedExecution(Runnable runnable, ThreadPoolExecutor executor) {
                if (!executor.isShutdown()) {
                    try {
                        executor.getQueue().put(runnable);
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                        Logger.getLogger(BatchMetricsChecker.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });
        
        try {
            String uri = null;
            final List<String> uris = params.get("-uri");
            if (uris == null || uris.isEmpty()) {
                try (InputStream in = BatchMetricsChecker.class.getClassLoader().getResourceAsStream("META-INF/config.properties")) {
                    if (in != null) {
                        final Properties properties = new Properties();
                        properties.load(in);
                        uri = properties.getProperty("mongodb.uri");
                    }
                } catch (IOException ex) {
                    Logger.getLogger(BatchMetricsChecker.class.getName()).log(Level.SEVERE, "no database URI found", ex);
                }
            } else {
                uri = uris.get(0);
            }
            
            new BatchMetricsChecker(executor).check(new MongoClientURI(uri)); 
        } finally {
            executor.shutdown();
        }

        System.exit(0);
    }

    public void check(MongoClientURI uri) {
        
        final String db = uri.getDatabase();
        
        Logger.getLogger(BatchMetricsChecker.class.getName()).log(Level.INFO, "connecting to {0}...", db);
        
        final MongoClient mc = new MongoClient(uri);
        
        final ToolsDAO toolsDAO = new ToolsDAO(mc.getDatabase(db), OpenEBenchEndpoint.TOOL_URI_BASE);
        final MetricsDAO metricsDAO = new MetricsDAO(mc.getDatabase(db), OpenEBenchEndpoint.METRICS_URI_BASE);
        
        final List<String> metrics_ids = metricsDAO.find(null, Sorts.ascending("project.website.last_check"));
        process(metrics_ids, toolsDAO, metricsDAO);
        
        final Set<String> tools_ids = toolsDAO.getIdentifiers();
        tools_ids.removeAll(metrics_ids);

        process(tools_ids, toolsDAO, metricsDAO);
                
        executor.shutdownNow();
        
        Logger.getLogger(BatchMetricsChecker.class.getName()).log(Level.INFO, "end.");
    }
    
    private void process(Collection<String> ids, 
                         ToolsDAO toolsDAO, 
                         MetricsDAO metricsDAO) {

        Logger.getLogger(BatchMetricsChecker.class.getName()).log(Level.INFO, "pushing {0} metrics.", ids.size());          

        CountDownLatch latch = new CountDownLatch(ids.size());

        for (String id : ids) {
            final Tool tool = toolsDAO.get(id);
            if (tool == null) {
                latch.countDown();
            } else {
                try {
                    final Future<Metrics> future = executor.submit(new MetricsCheckTask(tool, toolsDAO, metricsDAO));
                    executor.submit(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                final Metrics metrics = future.get(30, TimeUnit.MINUTES);
                                metricsDAO.merge("biotools", id, metrics);
                            } catch (Throwable th) {
                                Logger.getLogger(BatchMetricsChecker.class.getName()).log(Level.SEVERE, "update failed", th);
                            }
                            latch.countDown();
                        }
                    });
                } catch(Throwable th) {
                    Logger.getLogger(BatchMetricsChecker.class.getName()).log(Level.SEVERE, "submit failed", th);
                    latch.countDown();
                }
            }
        }

        // ensure that all taksks executed.
        try {
            latch.await(12, TimeUnit.HOURS);
        } catch (InterruptedException ex) {}

    }
    
    private static Map<String, List<String>> parameters(String[] args) {
        TreeMap<String, List<String>> parameters = new TreeMap();        
        List<String> values = null;
        for (String arg : args) {
            switch(arg) {
                case "-uri":  values = parameters.get(arg);
                              if (values == null) {
                                  values = new ArrayList(); 
                                  parameters.put(arg, values);
                              }
                              break;
                default: if (values != null) {
                    values.add(arg);
                }
            }
        }
        return parameters;
    }
}
